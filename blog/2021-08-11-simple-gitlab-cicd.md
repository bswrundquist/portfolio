---
slug: simple-gitlab-cicd
title: Setting up CI/CD on GitLab
author: Brandon Rundquist
<!-- author_title: bswrundquist -->
author_url: https://gitlab.com/bswr
tags: [gitlab, ci-cd, review-release, semantic-version]
---

Continuous integration is a set of tests that makes sure changes to a codebase does not introduce bugs. Continuous deployment follows by getting the code to a production state. GitLab offers a simple and native way to create pipelines that make this type of work possible.

<!--truncate-->

## Introduction

Through GitLab this post will walk through how to implement a simple continuous integration and deployment (from now on referred to as CI/CD) pipeline that will perform quality control, build a container, and push the image to a registry.

GitLab offers CI/CD as a service which can be defined simply as a _.gitlab-ci.yml_ file. This can be extended beyond a single file but that is out of scope for this post.

### Definitions and keywords

| Name                     | Short name     | Definition                                                        |
| ------------------------ | -------------- | ----------------------------------------------------------------- |
| Continuous integration   | CI             | Tasks that create confidence in the software                      |
| Continuous deployment    | CD             | Producing something of value that can be used                     |
| Container image          | Image          | Encapsulation of the environment required to run code             |
| Container image registry | Image registry | Where the container image is stored                               |
| Semantic versioning      | versioning     | Methodology for version bumping                                   |
| Review                   | N/A            | Code that is being worked on but can be scrutinized by developers |
| Release                  | N/A            | Code ready for use and is deployed with an audit trail            |

### Objective and assumptions

Listed are the _objectives_ of this post.

- Run code quality whenever code is pushed to GitLab
- Once code is ready for use, create an image and push it to a registry
- Bump the version based on commit message
- Create a release and a tag

The following _assumptions_ are being made.

- User has push access to a GitLab repository
- The default branch of the repository is called `main`
- Semantic versioning will be used for bumping version
- Version start with _v0.1.0_ so any version bump will begin from there

## Defining review and release

Categorizing the CI/CD into a **review** and **release** pipeline gives us a simple way of differentiating when code is being worked on and when it is completed. Normally this could be a myriad of categorizations that gradually build up to something that is ready to be deployed. Common names are dev, staging, qa, prod to name a few. This creates a more complicated deployment process that is not required for this exercise.

Code is in the state of **review** when it is not ready to be deployed but is being worked on.

Once a review is considered completed then it becomes a **release**. This is a snapshot of what the code is at the time of it being pushed to version control.

In this instance, the commit message is going to control whether the code push is happening on a review or a release. If the commit message contains the semantic version bump required (major, minor, patch) in brackets ([major], [minor], [patch]) then that is considered a release commit, otherwise it is a review commit.

| Type of commit | Example commit message                     |
| -------------- | ------------------------------------------ |
| Review         | chore: Small change, still more work       |
| Review         | fix: Change so now works                   |
| Release        | feat: [major] Adding a new breaking change |
| Release        | feat: [minor] New feature but no breaking  |
| Release        | feat: [patch] Quick hotfix                 |

Since we are going to use these rules multiple times we will take advantage of the [extends](https://docs.gitlab.com/ce/ci/yaml/#extends) feature offered by GitLab for reusing code in our `.gitlab-ci.yml` file. Using these rules each job can be communicate which pipeline it should be run with.

```yaml
.release-rules:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[(major|minor|patch)\]/
      when: always
    - when: never
```
Now whenever a commit message has either `[major]`, `[minor]`, or `[patch]` in it then it will be considered a release commit, everyting else is a review commit. To make a job run only on a release the `extends` parameter can be added to it and using `.release-rules`.

## Stages of the CI/CD pipeline

A `.gitlab-ci.yml` file is the default file that GitLab recognizes as the declaration in how the CI/CD pipeline should be constructed.
The CI/CD pipeline consists of two major parts, stages and jobs, that will be broken down further below.

A _stage_ is a job or a set of jobs that run in the order in which they are declared in the `stages:` section. Jobs in the same stage will run in parallel.
A job is where the actual work happens and consists of a set of arguments and command calls.
Each job is run inside a container so it can be tested locally with assurance it is working as expected.

```yaml title="CI/CD pipeline stages"
stages:
  - quality
  - prepare
  - deploy
  - release
```

Each of the subchapters below will detail what each job looks in detail.

### Code quality in parallel

Measuring code quality can not only ensure that code is meeting expected standards, but can also short circuit the pipeline if not.
There is an element of not having to run more than the minimal amount of jobs to save on cost and compute time and since quality measures are normally lightweight, it gives us a good opportunity to quit any jobs that do not overcome hurdles along it's way.

Two standard jobs that are done with minimal dependencies are **lint** and **static-type-check**. Linting and static type checking will not be detailed here but they are common measurements of code quality.

- lint
  - The `pylint` package to measure how much our code conforms to
    Python standards
  - Gives a value from -10 to 10
- static type check
  - The `mypy` package will run the type checking
  - Gives how many methods return and input the appropriately based on types

Linting and static type checking can be done independently which will be put in the same CI/CD stage and therefore run in parallel.

```yaml title="Code quality jobs"
lint:
  stage: quality
  script:
    - python3 -m pip install pylint pylint-exit
    - pylint main.py || pylint-exit $?

static-type-check:
  stage: quality
  script:
    - python3 -m pip install mypy
    - mypy main.py
```

:::tip
Most jobs are using Python so a default image can be used that will reduce the amount of boilerplate each job has. This YAML snippet will use Python version 3.8 as the base image for jobs unless they declare otherwise.

```yaml
default:
  image: python:3.8
```
:::

:::warning
The `pylint` package will fail in GitLab even if it returns successfully so the `pylint-exit` package is used to bypass this error.
:::

### Bump version

A release is going to require the version to be bumped based on what is contained within the commit message. This job will create the version that is required for a release to represent the type of changes that occurred within the codebase since the last release.

```yaml title="Job to bump version"
bump-version:
  stage: prepare
  script:
    - python3 -m pip install semver
    - git fetch --all --tags
    - CI_BUMP_TYPE=`echo $CI_COMMIT_MESSAGE | awk -F"[][]" '{print $2}'`
    - LATEST_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
    - CURRENT_VERSION=$((if [ -z $LATEST_TAG ]; then echo "v0.1.0"; else echo $LATEST_TAG; fi) | tr -d v)
    - BUMPED_VERSION=v$(pysemver bump $CI_BUMP_TYPE $CURRENT_VERSION)
    - echo "BUMPED_VERSION=${BUMPED_VERSION}" >> pipeline.env
  artifacts:
    reports:
      dotenv: pipeline.env
  extends:
    - .release-rules
```
These two lines will install the Python package [pysemver](https://python-semver.readthedocs.io/en/latest/index.html) which gives the ability to bump the version through the commanmd line. After that all the available tags are pulled (`git fetch --all --tags`) and then they are sorted by when they were commited and the latest one is made into the variable _LATEST_TAG_.

```yaml {4-6} title="Setup for bumping version"
bump-version:
  stage: prepare
  script:
    - python3 -m pip install semver
    - git fetch --all --tags
    - LATEST_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
    - CI_BUMP_TYPE=`echo $CI_COMMIT_MESSAGE | awk -F"[][]" '{print $2}'`
    - CURRENT_VERSION=$((if [ -z $LATEST_TAG ]; then echo "v0.1.0"; else echo $LATEST_TAG; fi) | tr -d v)
    - BUMPED_VERSION=v$(pysemver bump $CI_BUMP_TYPE $CURRENT_VERSION)
    - echo "BUMPED_VERSION=${BUMPED_VERSION}" >> pipeline.env
  artifacts:
    reports:
      dotenv: pipeline.env
  extends:
    - .release-rules
```

The code highlighted below is where the magic happens. First it extracts the type of semantic version bump required. Then it creates the current version based on the current `git` tag and if one is not found it defaults to _v0.1.0_.

```yaml {7-9} title="Bump version"
bump-version:
  stage: prepare
  script:
    - python3 -m pip install semver
    - git fetch --all --tags
    - LATEST_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
    - CI_BUMP_TYPE=`echo $CI_COMMIT_MESSAGE | awk -F"[][]" '{print $2}'`
    - CURRENT_VERSION=$((if [ -z $LATEST_TAG ]; then echo "v0.1.0"; else echo $LATEST_TAG; fi) | tr -d v)
    - BUMPED_VERSION=v$(pysemver bump $CI_BUMP_TYPE $CURRENT_VERSION)
    - echo "BUMPED_VERSION=${BUMPED_VERSION}" >> pipeline.env
  artifacts:
    reports:
      dotenv: pipeline.env
  extends:
    - .release-rules
```

:::note

The end of the CURRENT*VERSION variable trims off the leading *v* character (`tr -d v`). This makes it compatible with
the `pysemver` package. When the BUMPED*VERSION variable is created the leading *v* is added back.

:::

From there the environment variable called BUMPED_VERSION is created by pushing it into the file `pipeline.env` and save it as a *report* artifact of type *dotenv* which makes it an environment variable available to jobs downstream.

```yaml {10-13} title="Set environment variables for downstream jobs"
bump-version:
  stage: prepare
  script:
    - python3 -m pip install semver
    - git fetch --all --tags
    - LATEST_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
    - CI_BUMP_TYPE=`echo $CI_COMMIT_MESSAGE | awk -F"[][]" '{print $2}'`
    - CURRENT_VERSION=$((if [ -z $LATEST_TAG ]; then echo "v0.1.0"; else echo $LATEST_TAG; fi) | tr -d v)
    - BUMPED_VERSION=v$(pysemver bump $CI_BUMP_TYPE $CURRENT_VERSION)
    - echo "BUMPED_VERSION=${BUMPED_VERSION}" >> pipeline.env
  artifacts:
    reports:
      dotenv: pipeline.env
  extends:
    - .release-rules
```

Lastly, the final piece of this job is that it uses the GitLab `extends` feature to apply the `.release-rules` job that makes this job part of the release pipeline.

```yaml {14-15} title="Set rules for release pipeline"
bump-version:
  stage: prepare
  script:
    - python3 -m pip install semver
    - git fetch --all --tags
    - LATEST_TAG="$(git describe --tags `git rev-list --tags --max-count=1`)"
    - CI_BUMP_TYPE=`echo $CI_COMMIT_MESSAGE | awk -F"[][]" '{print $2}'`
    - CURRENT_VERSION=$((if [ -z $LATEST_TAG ]; then echo "v0.1.0"; else echo $LATEST_TAG; fi) | tr -d v)
    - BUMPED_VERSION=v$(pysemver bump $CI_BUMP_TYPE $CURRENT_VERSION)
    - echo "BUMPED_VERSION=${BUMPED_VERSION}" >> pipeline.env
  artifacts:
    reports:
      dotenv: pipeline.env
  extends:
    - .release-rules
```
### Build and push deployment

The deploy stage is where the code goes gets transformed into a usable artifact which in this case will be a container image that is stored in an registry.

Here is the CI/CD snippet that builds and pushes the container image to a repository using the [Kaniko](https://github.com/GoogleContainerTools/kaniko) package. Kaniko is a tool for building and pushing images and is more lightweight than using Docker.

```yaml title="Build and push image using Kaniko"
build-and-push-image:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - echo $BUMPED_VERSION
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --destination $CI_REGISTRY_IMAGE:$BUMPED_VERSION
  extends:
    - .release-rules
```

Kaniko uses the `/kaniko/.docker/config.json` file for credentials so they are inserted there using environment variables supplied by GitLab.

```yaml {7-8}
build-and-push-image:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - echo $BUMPED_VERSION
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --destination $CI_REGISTRY_IMAGE:$BUMPED_VERSION
  extends:
    - .release-rules
```

:::note Using a different image registry

Using another registry would require replacing the above step with the appropriate variables as a GitLab Secret. The names should also be changed since the `CI_*` variables come as part of the environment variables made available to CI/CD pipelines in GitLab.

:::

Then there is a call to the Kaniko executable that will push the image. We will define context using the project directory environment variable (CI_PROJECT_DIR) and define the destination to be the default GitLab image registry associated with the repository (CI_REGISTRY_IMAGE). Also notice the IMAGE_TAG variable is being used which was created from the `prepare` stage previously.

```yaml {9-13}
build-and-push-image:
  stage: deploy
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n ${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD} | base64)\"}}}" > /kaniko/.docker/config.json
    - echo $BUMPED_VERSION
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --destination $CI_REGISTRY_IMAGE:$BUMPED_VERSION
  extends:
    - .release-rules
```

### Finalize release

Now we can utilize GitLab's **release** job type to not only create a release but to also update the repository's tag with the latest version.
The field names under the _release_ section are what will populate a repository release as well as create a tag with the name `tag_name`.

```yaml
update-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Updating release to version $BUMPED_VERSION"
  release:
    name: "Release $BUMPED_VERSION"
    description: "$CI_COMMIT_MESSAGE"
    tag_name: "$BUMPED_VERSION"
  extends:
    - .release-rules
```

This will create a tag which will trigger the CI/CD pipeline to run again. To avoid the duplicate run the following can be added to not trigger when a tag is added to the repository.

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always
```

## Possible improvements

This is a simple CI/CD pipeline that could be improved. Here are a few items that could be expanded upon to make the pipeline more robust.

- Use a Merge Request title instead of a commit message to handle releases.
  - This will allow for letting a merge into the `main` branch handle when a release occurs.
- Create a review image
  - A review image can be created with an image tag that is the same as the branch name. This makes the review image available for testing.
- Add dependencies to CI/CD jobs
  - This will force downstream jobs to fail if a previous job fails


## Conclusion

Continuous integration and deployment is a powerful tool to make code both functional and auditable and GitLab offers a straightforward way of implementing it.
