const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

const math = require("remark-math");
const katex = require("rehype-katex");

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: "Some musings from over the years",
  tagline:
    "The philosophy section is under construction, see the notes section using the button below.",
  url: "https://bswrundquist.gitlab.io",
  baseUrl: "/portfolio/",
  onBrokenLinks: "warn",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/icons8-klein-bottle-64.png",
  organizationName: "bswrundquist", // Usually your GitHub org/user name.
  projectName: "portfolio", // Usually your repo name.
  stylesheets: [
    {
      href: "https://cdn.jsdelivr.net/npm/katex@0.13.11/dist/katex.min.css",
      integrity:
        "sha384-Um5gpz1odJg5Z4HAmzPtgZKdTBHZdw8S29IecapCSB31ligYPhHQZMIlWLYQGVoc",
      crossorigin: "anonymous",
    },
  ],
  themeConfig: {
    colorMode: {
      disableSwitch: true,
    },
    navbar: {
      title: "",
      logo: {
        alt: "",
        src: "img/klein-bottle.svg",
      },
      items: [
        {
          type: "doc",
          docId: "intro",
          position: "left",
          label: "Philosophy 🚧",
        },
        { to: "notes", label: "Notes", position: "left" },
        {
          href: "https://gitlab.com/bswrundquist/portfolio",
          label: "GitLab",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Personal",
          items: [
            {
              label: "GitLab Group",
              href: "https://gitlab.com/bswrundquist",
            },
            {
              label: "GitHub",
              href: "https://github.com/bswrundquist",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Built with Docusaurus.`,
    },
    prism: {
      theme: lightCodeTheme,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          remarkPlugins: [math],
          routeBasePath: "/philosophy",
          rehypePlugins: [[katex, { strict: false }]],
          showLastUpdateTime: true,
          // Please change this to your repo.
          editUrl: "https://bswrundquist.gitlab.io/portfolio/edit/main/docs",
        },
        blog: {
          path: "./blog",
          routeBasePath: "/notes",
          showReadingTime: true,
          // Please change this to your repo.
          editUrl: "https://bswrundquist.gitlab.io/portfolio/edit/blog",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
};
