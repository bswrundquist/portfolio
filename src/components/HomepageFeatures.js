import React from "react";
import clsx from "clsx";
import styles from "./HomepageFeatures.module.css";

const FeatureList = [
  {
    title: "A culture of collaboration",
    Svg: require("../../static/img/ramanujan-hardy-theorem.svg").default,
    description: (
      <>
        When collaboration is a core tenet of the culture then efforts become
        more powerful and gratifying.
      </>
    ),
  },
  {
    title: "Organization creates predictability",
    Svg: require("../../static/img/Linear_regression.svg").default,
    description: (
      <>
        When work is organized then the layout of what is happening becomes
        predictable and therefore more intuitive.
      </>
    ),
  },
  {
    title: "Keep it simple and repeatable",
    Svg: require("../../static/img/Eulers_formula.svg").default,
    description: (
      <>
        A simple process makes efforts easier to understand along with making
        repeatability more acheivable.
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        {/* <a href="https://example.com"> */}
        <Svg className={styles.featureSvg} alt={title} />
        {/* </a> */}
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
